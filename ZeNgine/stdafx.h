// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

//#include "targetver.h"
#include <SDKDDKVer.h>

#include <stdio.h>
#include <tchar.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdio.h>
#include <tchar.h>
#include <string>
#include <vector>
#include <fstream>
#include <memory>

#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>



using std::string;
using std::shared_ptr;
using std::make_shared;
using std::unique_ptr;
using std::make_unique;
using std::vector;
//using Vector2 = sf::Vector2<float>;
//using Vector3 = sf::Vector3<float>;
using Vector2 = glm::vec2;
using Vector3 = glm::vec3;
using Vector4 = glm::vec4;
using Matrix4 = glm::mat4;
using glm::translate;
using glm::scale;
using glm::rotate;
using glm::lookAt;
using glm::perspective;
using glm::radians;
//using glm::mat4;
//using glm::vec2;
//using glm::vec3;
//using glm::vec4;

using PrecisionType = float;

#include <spdlog/spdlog.h>
#include "common/log/ZLog.h"

#include "math/ZenMath.h"
//#include "math/Matrix4.hpp"
#include "util/debug/GLErrorUtil.hpp"
typedef GLuint uint;
