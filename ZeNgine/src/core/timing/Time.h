﻿// // /**
// //  * Time.h
// //  * Dylan Bailey
// //  * 20170522
// // */

#pragma once

namespace Zen
{
	class Time
	{
	public:
		static double GetTime();
		static double GetDeltaTime();

	private:
		static double lastTime;
		static double deltaTime;
		static double currentTime;
		static void UpdateFrame();

		friend class ZenEngine;
	};
}
