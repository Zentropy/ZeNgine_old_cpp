#include "stdafx.h"
#include "Material.h"
#include "core/rendering/shader/Shader.h"

namespace Zen
{
	int Material::textureBindings[16] = {GL_TEXTURE0, GL_TEXTURE1, GL_TEXTURE2, GL_TEXTURE3, GL_TEXTURE4, GL_TEXTURE5, GL_TEXTURE6, GL_TEXTURE7, GL_TEXTURE8, GL_TEXTURE9, GL_TEXTURE10, GL_TEXTURE11, GL_TEXTURE12, GL_TEXTURE13, GL_TEXTURE14, GL_TEXTURE15};

	int Material::CURRENT_SHADER_PROGRAM = -1;

	Material::Material(const string& inName, const string& vertex_file_path, const string& fragment_file_path)
	: name(inName)
	, shader (make_unique<Shader>(vertex_file_path, fragment_file_path))
	{
		
	}

	Material::~Material() {}

	bool Material::Use() {
		BindShader();		
		BindTextures();
		return true;
	}

	void Material::CreateTexture(const string& inImageFile, int inWidth, int inHeight, int inNumChannels /* = 0*/, int inForceChannels /*= SOIL_LOAD_RGB*/, bool generateTexture /*= true*/) {
		assert(textures.size() <= 16);
		textures.emplace_back(make_unique<Texture>(inImageFile, inWidth, inHeight, inNumChannels, inForceChannels, generateTexture));
	}

	void Material::BindShader()
	{
		auto pid = shader->GetProgramID();
		if (pid != CURRENT_SHADER_PROGRAM) {
			shader->Use();
			CURRENT_SHADER_PROGRAM = pid;
		}
	}



	void Material::BindTextures() {
		assert(textures.size() <= 16);

		for (int i = 0; i < textures.size(); ++i) {
			glActiveTexture(textureBindings[i]);
			glBindTexture(GL_TEXTURE_2D, textures[i]->textureID);
		}		
	}
}
