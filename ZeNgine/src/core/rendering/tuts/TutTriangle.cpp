﻿#include "stdafx.h"
#include "TutTriangle.h"
#include "util/debug/GLErrorUtil.hpp"
#include "core/rendering/shader/Shader.h"
#include "soil2/SOIL2.h"
#include "core/rendering/texture/Texture.h"
#include "../material/Material.h"
#include "core/ZenEngine.h"
#include "core/input/PlayerInput.h"
#include "core/timing/Time.h"

namespace Zen
{	
	TutTriangle::TutTriangle()
		:vertices{
		Vertex(-0.5f, -0.5f, -0.5f,  0.0f, 0.0f),
		Vertex(0.5f, -0.5f, -0.5f,  1.0f, 0.0f),
		Vertex(0.5f,  0.5f, -0.5f,  1.0f, 1.0f),
		Vertex(0.5f,  0.5f, -0.5f,  1.0f, 1.0f),
		Vertex(-0.5f,  0.5f, -0.5f,  0.0f, 1.0f),
		Vertex(-0.5f, -0.5f, -0.5f,  0.0f, 0.0f),
		
		Vertex(-0.5f, -0.5f,  0.5f,  0.0f, 0.0f),
		Vertex(0.5f, -0.5f,  0.5f,  1.0f, 0.0f),
		Vertex(0.5f,  0.5f,  0.5f,  1.0f, 1.0f),
		Vertex(0.5f,  0.5f,  0.5f,  1.0f, 1.0f),
		Vertex(-0.5f,  0.5f,  0.5f,  0.0f, 1.0f),
		Vertex(-0.5f, -0.5f,  0.5f,  0.0f, 0.0f),
		
		Vertex(-0.5f,  0.5f,  0.5f,  1.0f, 0.0f),
		Vertex(-0.5f,  0.5f, -0.5f,  1.0f, 1.0f),
		Vertex(-0.5f, -0.5f, -0.5f,  0.0f, 1.0f),
		Vertex(-0.5f, -0.5f, -0.5f,  0.0f, 1.0f),
		Vertex(-0.5f, -0.5f,  0.5f,  0.0f, 0.0f),
		Vertex(-0.5f,  0.5f,  0.5f,  1.0f, 0.0f),
		Vertex(0.5f,  0.5f,  0.5f,  1.0f, 0.0f),
		Vertex(0.5f,  0.5f, -0.5f,  1.0f, 1.0f),
		Vertex(0.5f, -0.5f, -0.5f,  0.0f, 1.0f),
		Vertex(0.5f, -0.5f, -0.5f,  0.0f, 1.0f),
		Vertex(0.5f, -0.5f,  0.5f,  0.0f, 0.0f),
		Vertex(0.5f,  0.5f,  0.5f,  1.0f, 0.0f),
		Vertex(-0.5f, -0.5f, -0.5f,  0.0f, 1.0f),
 		 Vertex(0.5f, -0.5f, -0.5f,  1.0f, 1.0f),
		 Vertex(0.5f, -0.5f,  0.5f,  1.0f, 0.0f),
		 Vertex(0.5f, -0.5f,  0.5f,  1.0f, 0.0f),
		Vertex(-0.5f, -0.5f,  0.5f,  0.0f, 0.0f),
		Vertex(-0.5f, -0.5f, -0.5f,  0.0f, 1.0f),
		Vertex(-0.5f,  0.5f, -0.5f,  0.0f, 1.0f),
		Vertex(0.5f,  0.5f, -0.5f,  1.0f, 1.0f),
		Vertex(0.5f,  0.5f,  0.5f,  1.0f, 0.0f),
		Vertex(0.5f,  0.5f,  0.5f,  1.0f, 0.0f),
		Vertex(-0.5f,  0.5f,  0.5f,  0.0f, 0.0f),
		Vertex(-0.5f,  0.5f, -0.5f,  0.0f, 1.0f)
		}, 
		indices{
		0, 1, 3, //quad
		1, 2, 3} //quad
		//0,1,2 } // Triangle
	{
		Init();
	}

	TutTriangle::~TutTriangle() {
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO);
	}

	void TutTriangle::Init() {
		GLErrorUtils::TestForErrors();
		//shaderProgram = Shader::LoadShaders("BaseVertexShader.vert", "BaseFragmentShader.frag");
		//triangleShader = make_unique<Shader>("BaseVertexShader.vert", "BaseFragmentShader.frag");
		//bookcaseTexture = make_unique<Texture>("container.jpg", 512, 512);
		//smileyTexture = make_unique<Texture>("awesomeface.png", 512, 512);
		tutMaterial = make_unique<Material>("Tutorial Material","BaseVertexShader.vert", "BaseFragmentShader.frag");
		tutMaterial->CreateTexture("container.jpg", 512, 512);
		tutMaterial->CreateTexture("awesomeface.png", 512, 512);
		// set up vertex data (and buffer(s)) and configure vertex attributes

		GLErrorUtils::LogError();
		glGenVertexArrays(1, &VAO);
		GLErrorUtils::LogError();
		glBindVertexArray(VAO);		
		GLErrorUtils::LogError();
		
		glGenBuffers(1, &EBO);
		GLErrorUtils::LogError();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		GLErrorUtils::LogError();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
		GLErrorUtils::LogError();
		glGenBuffers(1, &VBO);
		GLErrorUtils::LogError();
		// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
		
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		GLErrorUtils::LogError();
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
		GLErrorUtils::LogError();
		
		//position info
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
		GLErrorUtils::LogError();
		glEnableVertexAttribArray(0);
		GLErrorUtils::LogError();
		//color attribute
		//glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(3 * sizeof(GLfloat)));
		//glEnableVertexAttribArray(1);
		//Texture coord attribute
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(2);

		GLErrorUtils::LogError();
		
		
		// note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
		// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
		glBindVertexArray(0);
		GLErrorUtils::LogError();

		//model matrix stuff
		//view = translate(view, Vector3(0.0f, 0.0f, -3.0f));
		projection = perspective(radians(45.0f), 800.0f/600, 0.1f, 100.0f);

		camera = make_unique<Camera>();
		//camera->SetPosition(0, 0, 3.0f);
		//camera->SetTarget(0.0f, 0.0f, 0.0f);
		//view = camera->GetLookAt();
	}

	void TutTriangle::HandleCameraUpdate() {

		//float y = PlayerInput::mousePositionDelta.y;
		//float x = PlayerInput::mousePositionDelta.x;
		//
		//if (std::abs(y) > 0.01f)
		//	pitch += PlayerInput::mousePositionDelta.y;
		//if (std::abs(x) > 0.01f)
		//	yaw += PlayerInput::mousePositionDelta.x;
		//
		//if(pitch > 89.0f)
		//	pitch =  89.0f;
		//if(pitch < -89.0f)
		//	pitch = -89.0f;

		//glm::vec3 front;
		//front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
		//front.y = sin(glm::radians(pitch));
		//front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
		//
		////if (PlayerInput::QPressed) {
		//	camera->GetForward() = glm::normalize(front);
		//	auto f = camera->GetForward();
		//	LOG_DEBUG("Pitch: {} - Yaw: {} - Forward: {} , {} , {}", pitch, yaw, f.x, f.y, f.z);
		//}
		camera->ProcessMouseMovement();
	}

	void TutTriangle::Render() {
		
		GLErrorUtils::LogError();

		HandleCameraInput();
		HandleCameraUpdate();
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		//translate test
		//Vector4 transvec(1, 0, 0, 1);
		//glm::mat4 trans;
		//trans = glm::rotate(trans, glm::radians((GLfloat)glfwGetTime() * 50.0f), glm::vec3(0.0, 0.0, 1.0));
		//trans = glm::scale(trans, glm::vec3(0.5, 0.5, 0.5));
		//
		//uint transformLoc = tutMaterial->shader->GetUniformLocation("transform");
		//glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(trans));		
		//GLErrorUtils::LogError();

		glBindVertexArray(VAO); 
		//glDrawArrays(GL_TRIANGLES, 0, 3); 
		GLErrorUtils::LogError();
		
		tutMaterial->Use();
		GLErrorUtils::LogError();
		GLuint vertexColorLocation = tutMaterial->shader->GetUniformLocation("texMix");
		GLErrorUtils::LogError();		
		//glUniform1f(vertexColorLocation, ZenEngine::keypress);
		glUniform1f(vertexColorLocation, 0.3f);

		//MVP Construction
		model = rotate(model, radians((GLfloat)Time::GetDeltaTime() * 50.050f), Vector3(0.0f, 0.50f, 0.50f));
		view = camera->GetViewMatrix();

		//MVP sending
		GLint modelLoc = tutMaterial->shader->GetUniformLocation("model");
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, value_ptr(model));

		modelLoc = tutMaterial->shader->GetUniformLocation("view");
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, value_ptr(view));

		modelLoc = tutMaterial->shader->GetUniformLocation("projection");
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, value_ptr(projection));

		GLErrorUtils::LogError();
		//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		
	}

	void TutTriangle::HandleCameraInput()
	{
		camera->ProcessKeyboard();
		//float camSpeed = 5.f * Time::GetDeltaTime();
		//
		//
		//if(PlayerInput::UpPressed) 
		//	camera->GetPosition() += camera->GetForward() * camSpeed;
		//if(PlayerInput::DownPressed) 
		//	camera->GetPosition() -= camera->GetForward() * camSpeed;
		//if(PlayerInput::LeftPressed) 
		//	camera->GetPosition() -= camera->GetRight() * camSpeed;
		//if(PlayerInput::RightPressed) 
		//	camera->GetPosition() += camera->GetRight() * camSpeed;
		//if(PlayerInput::QPressed) 
		//	camera->GetPosition().z += Time::GetDeltaTime() * camSpeed;
		//if(PlayerInput::EPressed) 
		//	camera->GetPosition().z -= Time::GetDeltaTime() * camSpeed;

		
	}

}
