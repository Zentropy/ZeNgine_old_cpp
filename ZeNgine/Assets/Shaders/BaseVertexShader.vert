#version 450

layout(location = 0) in vec3 Position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec2 texCoord;

out vec4 ourColor;
out vec2 TexCoord;

//uniform mat4 transform;
//uniform float texMix;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


void main()
{
	//gl_Position = transform * vec4(Position.x, Position.y, Position.z, 1.0);
	gl_Position = projection * view * model * vec4(Position, 1.0f);
	ourColor = color;
	TexCoord = texCoord;
}
