﻿// // /**
// //  * ShaderManager.h
// //  * Dylan Bailey
// //  * 20170429
// // */

#pragma once
#include <GL/glew.h>


namespace Zen
{
	class Shader
	{
	public:
		Shader(const string& vertex_file_path, const string& fragment_file_path, bool inUseAssetPath = true);
		void Use();
		GLuint GetProgramID() { return programID; }
		GLuint GetUniformLocation(const string& locationName);

	private:
		GLuint programID;
		bool useAssetPath;
		const string assetPath = "Assets/Shaders/";
		string vertPath, fragPath;
	};
}
