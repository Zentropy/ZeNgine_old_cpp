#pragma once
#include "core/rendering/texture/Texture.h"

namespace Zen {
	class Shader;
	class Material
	{
	public:
		Material(const string& name, const string& vertex_file_path, const string& fragment_file_path);
		~Material();
		bool Use();

		void BindShader();

		void CreateTexture(const string& inImageFile, int inWidth, int inHeight, int inNumChannels = 0, int inForceChannels = SOIL_LOAD_RGB, bool generateTexture = true);

		size_t GetTextureCount() {
			return textures.size();
		}

		unique_ptr<Shader> shader;

	private:
		void BindTextures();

		string name;
		vector<unique_ptr<Texture>> textures;
		static int textureBindings[16]; 
		static int CURRENT_SHADER_PROGRAM;
	};
}