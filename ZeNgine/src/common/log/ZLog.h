#pragma once
#include <spdlog/logger.h>

namespace Zen {

	class ZLog
	{
	public:
		static shared_ptr<spdlog::logger> Logger;
	};
}
