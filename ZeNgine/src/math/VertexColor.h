﻿// // /**
// //  * Vertex.h
// //  * Dylan Bailey
// //  * 20170514
// // */

#pragma once

namespace Zen
{
	class VertexColor
	{
	public:
		Vector3 position;
		Vector4 color;

		VertexColor();
		VertexColor(Vector3 inPosition, Vector4 inColor);
		VertexColor(float x, float y, float z, float r = 1.0f, float g = 1.0f, float b = 1.0f, float a = 1.0f);

	};
}
