﻿// // /**
// //  * ZenEngine.h
// //  * Dylan Bailey
// //  * 20170513
// // */

#pragma once
#include "core/rendering/Renderer.h"

namespace Zen
{
	class ZenEngine
	{
	public:
		bool Init();

		bool InitOpenGL();

		void Run();
		void Start();

		//void framebuffer_size_callback(GLFWwindow* window, int width, int height);
		void processInput(GLFWwindow *window);

		//static float keypress;

		GLFWwindow* window;
		const GLuint WIDTH = 800, HEIGHT = 600;
		unique_ptr<Renderer> renderer;
	};
}
