﻿// // /**
// //  * Vertex.h
// //  * Dylan Bailey
// //  * 20170514
// // */

#pragma once

namespace Zen
{
	class Vertex
	{
	public:
		Vector3 position;
		Vector2 textureCoords;

		Vertex();
		Vertex(Vector3 inPosition, Vector2 inTextureCoords);
		Vertex(float x, float y, float z, float textureX = 0.0f, float textureY = 0.0f);

	};
}
