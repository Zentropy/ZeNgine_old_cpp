﻿#include "stdafx.h"
#include "PlayerInput.h"


namespace Zen
{
	bool PlayerInput::LeftPressed = false;
	bool PlayerInput::RightPressed = false;
	bool PlayerInput::UpPressed = false;
	bool PlayerInput::DownPressed = false; 
	bool PlayerInput::QPressed = false;
	bool PlayerInput::EPressed = false;
	bool PlayerInput::ZPressed = false;
	bool PlayerInput::XPressed = false;


	
	Vector2 PlayerInput::mousePositionCurrent;

	Vector2 PlayerInput::mousePositionPrevious = Vector2(400, 300);
	Vector2 PlayerInput::mousePositionDelta;

	void PlayerInput::Init(GLFWwindow* window) {
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);  
		glfwSetCursorPosCallback(window, mouse_callback);
	}

	Vector2 PlayerInput::GetMousePositionDelta()
	{
		//Vector2 delta = (mousePositionCurrent - mousePositionPrevious) * mouseSensitivity;
		//LOG_DEBUG("Mouse delta is: {}, {}", delta.x, delta.y);
		return mousePositionDelta;
	}

	void PlayerInput::ProcessInput(GLFWwindow* window)
	{
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);

		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
			QPressed = true;
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
			EPressed = true;
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_RELEASE)
			QPressed = false;
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_RELEASE)
			EPressed = false;

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			UpPressed = true;
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			DownPressed = true;
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			LeftPressed = true;
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			RightPressed = true;

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_RELEASE)
			UpPressed = false;
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_RELEASE)
			DownPressed = false;
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_RELEASE)
			LeftPressed = false;
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_RELEASE)
			RightPressed = false;

		if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS)
			ZPressed = true;
		if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS)
			XPressed = true;
		if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_RELEASE)
			ZPressed = false;
		if (glfwGetKey(window, GLFW_KEY_X) == GLFW_RELEASE)
			XPressed = false;

		//keypress = std::clamp(keypress, 0.0f, 1.0f);
	}

	bool firstMouse = true;
	void PlayerInput::mouse_callback(GLFWwindow* window, double xpos, double ypos)
	{
		if (firstMouse) {
			mousePositionPrevious.x = xpos;
			mousePositionPrevious.y = ypos;
			firstMouse = false;
		}

		mousePositionDelta.x = xpos - mousePositionPrevious.x;
		mousePositionDelta.y = mousePositionPrevious.y - ypos; //reversed to flip y coords
		mousePositionPrevious = mousePositionCurrent;
		mousePositionCurrent.x = xpos;
		mousePositionCurrent.y = ypos;

		mouseSensitivity = 0.05f;
		mousePositionDelta.x *= mouseSensitivity;
		mousePositionDelta.y *= mouseSensitivity;
		//LOG_DEBUG("Mouse Pos: {} , {} ", xpos, ypos);
	}

	float PlayerInput::mouseSensitivity = 50.5f;

}
