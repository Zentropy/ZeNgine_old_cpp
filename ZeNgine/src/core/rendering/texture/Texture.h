#pragma once
#include "soil2/SOIL2.h"
namespace Zen {

	class Texture
	{
	public:
		Texture(const string& inImageFile, int inWidth, int inHeight, int inNumChannels = 0, int inForceChannels = SOIL_LOAD_RGB, bool generateTexture = true);
		~Texture();

		GLuint textureID;

	private:
		unsigned char* image;
		int width, height, numChannels, forceChannels;		
	};
}