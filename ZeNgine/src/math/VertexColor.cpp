﻿#include "stdafx.h"
#include "VertexColor.h"

namespace Zen
{
	VertexColor::VertexColor()
		: position(0.0f, 0.0f, 0.0f)
		, color(1.0f, 1.0f, 1.0f, 1.0f)
	{}

	VertexColor::VertexColor(Vector3 inPosition, Vector4 inColor) 
		: position(inPosition), color(inColor)
	{}

	VertexColor::VertexColor(float x, float y, float z, float r, float g, float b, float a) 
		: position{ x, y, z }, color{ r, g, b, a }
	{}
}
