﻿// // /**
// //  * PlayerInput.h
// //  * Dylan Bailey
// //  * 20170521
// // */

#pragma once

namespace Zen
{
	class PlayerInput
	{
	public:
		static bool LeftPressed;
		static bool RightPressed;
		static bool UpPressed;
		static bool DownPressed; 
		static bool QPressed;
		static bool EPressed;
		static bool ZPressed;
		static bool XPressed;
		static Vector2 mousePositionCurrent;
		static Vector2 mousePositionPrevious;
		static Vector2 mousePositionDelta;
		static Vector2 GetMousePositionDelta();

		static void Init(GLFWwindow* window);
		static void ProcessInput(GLFWwindow* window);
		static void mouse_callback(GLFWwindow* window, double xpos, double ypos);

	private:
		static float mouseSensitivity;
	};
}
