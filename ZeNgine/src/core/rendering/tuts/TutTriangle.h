﻿// // /**
// //  * Tut2.h
// //  * Dylan Bailey
// //  * 20170513
// // */

#pragma once

#include "core/rendering/interfaces/IRenderable.hpp"
#include "math/Vertex.h"
#include "core/rendering/camera/Camera.h"

namespace Zen
{
	class Shader;
	class Texture;
	class Material;

	class TutTriangle : public IRenderable
	{
	public:
		TutTriangle();
		~TutTriangle();
		void Init();
		void HandleCameraUpdate();
		void Render() override;
		uint VBO, VAO, EBO;

		unique_ptr<Camera> camera;
		
		Vertex vertices[36];
		GLuint indices[6];
		//unique_ptr<Shader> triangleShader;
		//unique_ptr<Texture> bookcaseTexture, smileyTexture;
		unique_ptr<Material> tutMaterial;
		
		Matrix4 model, view, projection;
	private:
		void HandleCameraInput();

		float pitch, yaw = -90.0f;
	};
}
