﻿#include "stdafx.h"
#include "Vertex.h"

namespace Zen
{
	Vertex::Vertex()
		: position(0.0f, 0.0f, 0.0f)
		, textureCoords(0.0f, 0.0f)
	{}

	Vertex::Vertex(Vector3 inPosition, Vector2 inTextureCoords) 
		: position(inPosition), textureCoords(inTextureCoords)
	{}

	Vertex::Vertex(float x, float y, float z, float texX, float texY) 
		: position{ x, y, z },  textureCoords(texX, texY)
	{}
}
