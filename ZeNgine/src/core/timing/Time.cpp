﻿#include "stdafx.h"
#include "Time.h"

namespace Zen {

	double Time::lastTime;

	double Time::deltaTime;

	double Time::currentTime;
	
	double Time::GetTime()
	{
		return currentTime;
	}

	double Time::GetDeltaTime()
	{
		return deltaTime;
	}

	void Time::UpdateFrame()
	{
		lastTime = currentTime;
		currentTime = glfwGetTime();
		deltaTime = currentTime - lastTime;
	}

}