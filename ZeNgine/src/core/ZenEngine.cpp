﻿#include "stdafx.h"
#include "ZenEngine.h"
#include "util/debug/GLErrorUtil.hpp"
#include "core/rendering/shader/Shader.h"
#include <algorithm>
#include "core/input/PlayerInput.h"
#include "core/timing/Time.h"

//#include <IL/il2.h>
//#include <IL/ilu2.h>
//#include <IL/ilut.h>
//#include <glload/gl_all.hpp>
//#include <glload/gll.hpp>
//#include <glimg/glimg.h>

namespace Zen
{
	void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
		// make sure the viewport matches the new window dimensions; note that width and 
		// height will be significantly larger than specified on retina displays.
		glViewport(0, 0, width, height);
	}

	bool ZenEngine::Init() {
		ZLog::Logger->set_level(spdlog::level::trace);
		
		InitOpenGL();
		
		renderer = make_unique<Renderer>(window);
		renderer->Init();
		
		LOG_DEBUG("Renderer initialized");
		PlayerInput::Init(window);
		return true;
	}

	bool ZenEngine::InitOpenGL()
	{
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
		glfwWindowHint(GLFW_RESIZABLE, false);

		window = glfwCreateWindow(WIDTH, HEIGHT, "ZeNgine", nullptr, nullptr);
		glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

		int screenWidth, screenHeight;
		glfwGetFramebufferSize(window, &screenWidth, &screenHeight);

		if (nullptr == window) {
			LOG_TRACE("Failed to create GLFW Window\n");
			glfwTerminate();
			return false;
		}

		glfwMakeContextCurrent(window);
		glewExperimental = GL_TRUE;

		if (GLEW_OK != glewInit()) {
			LOG_TRACE("Glew failed to init properly\n");
			return false;
		}

		glViewport(0, 0, screenWidth, screenHeight);
		return true;
	}

	void ZenEngine::Start() {
		if (Init()) {
			glfwSetTime(0.0);
			Run();
		}
		else {
			LOG_TRACE("INIT FAILED");
			return;
		}

		glfwTerminate();
		return;
	}

	//MAIN LOOP
	void ZenEngine::Run() {

		// uncomment this call to draw in wireframe polygons.
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		// render loop 		
		while (!glfwWindowShouldClose(window)) {
			Time::UpdateFrame();
			processInput(window);
			renderer->Render();
			
		}
	}
	
	void ZenEngine::processInput(GLFWwindow* window) {
		PlayerInput::ProcessInput(window);
	}


}
