#version 450

in vec2 TexCoord;
in vec4 ourColor;

out vec4 FragColor;

layout(binding=0) uniform sampler2D ourTexture;
layout(binding=1) uniform sampler2D ourOtherTexture;

uniform float texMix;

void main()
{
	//FragColor = texture(ourTexture, TexCoord) * ourColor;
	FragColor = mix(texture(ourTexture, TexCoord), texture(ourOtherTexture, TexCoord), texMix);
	//FragColor = ourColor;
}
