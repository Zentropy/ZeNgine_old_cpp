#include "stdafx.h"
#include "Renderer.h"
#include "util/debug/GLErrorUtil.hpp"
#include "core/rendering/shader/Shader.h"

namespace Zen
{
	
	Renderer::Renderer(GLFWwindow* inWindow) 	
		: renderList{ vector<unique_ptr<IRenderable>>() }
	{
		window = inWindow;
	}
	Renderer::~Renderer() {
		
	}

	//void Renderer::BindDefaultVertexArray() {
	//	//MUST generate at least one vertex array or GL won't display anything!!!!!
	//	glGenVertexArrays(1, &VAO);
	//	GLErrorUtils::LogError();
	//	glBindVertexArray(VAO);
	//	GLErrorUtils::LogError();
	//}

	void Renderer::Init() {
		//renderList.emplace_back(new TutTriangle());
		renderList.emplace_back(make_unique<TutTriangle>());
		glEnable(GL_DEPTH_TEST);
		
	}

	void Renderer::Render() {
	
		// render
		// ------
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		for (int i = 0; i < renderList.size(); ++i) {
			renderList[i]->Render();
		}
		// draw our first triangle
		
		// glBindVertexArray(0); // no need to unbind it every time 

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		GLErrorUtils::LogError();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
}
