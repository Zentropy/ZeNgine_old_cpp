#include "stdafx.h"
#include "Texture.h"
#include "util\debug\GLErrorUtil.hpp"


namespace Zen {

	Texture::Texture(const string& inImageFile, int inWidth, int inHeight, int inNumChannels /* = 0*/, int inForceChannels /*= SOIL_LOAD_RGB*/, bool generateTexture /*= true*/)
		: width(inWidth), height(inHeight), numChannels(inNumChannels), forceChannels(inForceChannels)
	{	
		std::string filePath = "Assets/Textures/" + inImageFile;
		image = SOIL_load_image(filePath.c_str(), &width, &height, &numChannels, forceChannels);
		
		glGenTextures(1, &textureID);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(image);
		glBindTexture(GL_TEXTURE_2D, 0);
		GLErrorUtils::LogError();
	
	}

	Texture::~Texture()
	{
	}

	
}
