﻿#include "stdafx.h"
#include "Shader.h"

namespace Zen
{


	Shader::Shader(const string& vertex_file_path, const string& fragment_file_path, bool inUseAssetPath)
		: useAssetPath(inUseAssetPath)
	{
		GLErrorUtils::LogError();
		
		if (useAssetPath) {
			vertPath = assetPath + vertex_file_path;
			fragPath = assetPath + fragment_file_path;
		}
		else {
			vertPath = vertex_file_path;
			fragPath = fragment_file_path;
		}

		// Create the shaders
		GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

		// Read the Vertex Shader code from the file
		string VertexShaderCode;
		std::ifstream VertexShaderStream(vertPath, std::ios::in);
		if (VertexShaderStream.is_open()) {
			string Line = "";
			while (getline(VertexShaderStream, Line))
				VertexShaderCode += "\n" + Line;
			VertexShaderStream.close();
		}
		else {
			LOG_TRACE("Impossible to open {}. Are you in the right directory?", vertex_file_path);
			return;			
		}

		// Read the Fragment Shader code from the file
		string FragmentShaderCode;
		std::ifstream FragmentShaderStream(fragPath, std::ios::in);
		if (FragmentShaderStream.is_open()) {
			string Line = "";
			while (getline(FragmentShaderStream, Line))
				FragmentShaderCode += "\n" + Line;
			FragmentShaderStream.close();
		}

		GLint Result = GL_FALSE;
		int InfoLogLength;

		// Compile Vertex Shader
		LOG_DEBUG("Compiling shader : {}", vertex_file_path);
		char const* VertexSourcePointer = VertexShaderCode.c_str();
		glShaderSource(VertexShaderID, 1, &VertexSourcePointer, nullptr);
		glCompileShader(VertexShaderID);

		// Check Vertex Shader
		glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if (InfoLogLength > 0) {
			vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
			glGetShaderInfoLog(VertexShaderID, InfoLogLength, nullptr, &VertexShaderErrorMessage[0]);
			LOG_TRACE("{}", &VertexShaderErrorMessage[0]);
		}

		// Compile Fragment Shader
		LOG_DEBUG("Compiling shader : {}", fragment_file_path);
		char const* FragmentSourcePointer = FragmentShaderCode.c_str();
		glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, nullptr);
		glCompileShader(FragmentShaderID);

		// Check Fragment Shader
		glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if (InfoLogLength > 0) {
			vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
			glGetShaderInfoLog(FragmentShaderID, InfoLogLength, nullptr, &FragmentShaderErrorMessage[0]);
			LOG_TRACE("Shader Error: {}", &FragmentShaderErrorMessage[0]);
		}

		// Link the program
		LOG_DEBUG("Linking program\n");
		programID = glCreateProgram();
		glAttachShader(programID, VertexShaderID);
		glAttachShader(programID, FragmentShaderID);
		glLinkProgram(programID);

		// Check the program
		glGetProgramiv(programID, GL_LINK_STATUS, &Result);
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if (InfoLogLength > 0) {
			vector<char> ProgramErrorMessage(InfoLogLength + 1);
			glGetProgramInfoLog(programID, InfoLogLength, nullptr, &ProgramErrorMessage[0]);
			LOG_TRACE("Shader Error: {}", &ProgramErrorMessage[0]);
		}

		//Detach and delete the shader objects from the program now that they're properly linked
		glDetachShader(programID, VertexShaderID);
		glDetachShader(programID, FragmentShaderID);

		glDeleteShader(VertexShaderID);
		glDeleteShader(FragmentShaderID);
		GLErrorUtils::LogError();
	}

	void Shader::Use() {
		GLErrorUtils::LogError();
		glUseProgram(programID);
		GLErrorUtils::LogError();
	}

	GLuint Shader::GetUniformLocation(const string& locationName)
	{
		return glGetUniformLocation(programID, locationName.c_str());
	}

}
