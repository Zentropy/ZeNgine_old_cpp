#pragma once
#include "GL\glew.h"
#include "spdlog\spdlog.h"

namespace Zen
{
	class GLErrorUtils
	{
	public: 
		static void DisplayGLError(GLenum error) {
			switch (error) {
			case GL_INVALID_ENUM:
				LOG_DEBUG("GL INVALID ENUM");
				break;
			case GL_INVALID_VALUE:
				LOG_DEBUG("GL INVALID VALUE");
				break;
			case GL_INVALID_OPERATION :
				LOG_DEBUG("gl invalid operation");
				break;
			case GL_STACK_OVERFLOW:
				LOG_DEBUG("gl stack overflow");
				break;
			case GL_STACK_UNDERFLOW:
				LOG_DEBUG("gl stack underflow");
				break;
			case GL_OUT_OF_MEMORY:
				LOG_DEBUG("gl out of memory");
				break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:
				LOG_DEBUG("gl invalid framebuffer operation");
				break;
			case GL_CONTEXT_LOST:
				LOG_DEBUG("gl context lost");
				break;
			case GL_TABLE_TOO_LARGE:
				LOG_DEBUG("gl table too large");
				break;
			default:
				LOG_DEBUG("Unhandled GL Enum error");
				break;
			}
		}

		static void TestForErrors() {
			GLenum err = glGetError();
			
			while (err != GL_NO_ERROR) {
				DisplayGLError(err);
				err = glGetError();
			}
		}

		static void LogError() {
			auto err = glGetError();
			if (err != 0) {
				std::cout << "Error is " << err << std::endl;
			}
		}
	};
}