﻿// // /**
// //  * Transform.h
// //  * Dylan Bailey
// //  * 20170520
// // */

#pragma once

namespace Zen
{
	class Transform
	{
	public:
		void SetPosition(Vector3 inPosition);
		void SetPosition(float x, float y, float z);
		Vector3 GetPosition();

		void SetRotation(Quaternion inRotation);
		Quaternion GetRotation();
		
		void SetScale(Vector3 inScale);
		void SetScale(float x, float y, float z);
		Vector3 GetScale();

		//Matrix4 GetTransformMatrix();

	private:
		Vector3 position;
		Quaternion rotation;
		Vector3 scale;
		Matrix4 transformMatrix;
		bool matrixIsDirty = true;
	};
}
