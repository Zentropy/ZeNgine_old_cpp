﻿// // /**
// //  * Camera.h
// //  * Dylan Bailey
// //  * 20170521
// // */

#pragma once
#include "core/input/PlayerInput.h"
#include "core/timing/Time.h"

namespace Zen
{
	const GLfloat YAW        = -90.0f;
	const GLfloat PITCH      =  0.0f;
	const GLfloat SPEED      =  2.5f;
	const GLfloat SENSITIVTY =  2.1f;
	const GLfloat ZOOM       =  45.0f;

	class Camera
	{
	public:
		// Camera Attributes
		glm::vec3 position;
		glm::vec3 forward;
		glm::vec3 up;
		glm::vec3 right;
		glm::vec3 worldUp;
		// Eular Angles
		GLfloat yaw;
		GLfloat pitch;
		// Camera options
		GLfloat movementSpeed;
		GLfloat mouseSensitivity;
		GLfloat zoom;
		GLfloat invertYLook;

		// Constructor with vectors
		Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH) 
		: forward(glm::vec3(0.0f, 0.0f, -1.0f)), movementSpeed(SPEED), mouseSensitivity(SENSITIVTY), zoom(ZOOM), invertYLook(-1.0f)
		{
			this->position = position;
			this->worldUp = up;
			this->yaw = yaw;
			this->pitch = pitch;
			this->updateCameraVectors();
		}
		// Constructor with scalar values
		Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch) : forward(glm::vec3(0.0f, 0.0f, -1.0f)), movementSpeed(SPEED), mouseSensitivity(SENSITIVTY), zoom(ZOOM)
		{
			this->position = glm::vec3(posX, posY, posZ);
			this->worldUp = glm::vec3(upX, upY, upZ);
			this->yaw = yaw;
			this->pitch = pitch;
			this->updateCameraVectors();
		}

		// Returns the view matrix calculated using Eular Angles and the LookAt Matrix
		glm::mat4 GetViewMatrix()
		{
			return lookAt(this->position, this->position + this->forward, this->up);
		}

		// Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
		void ProcessKeyboard()
		{
			float camSpeed = 5.f * Time::GetDeltaTime();

			if(PlayerInput::UpPressed) 
				position += forward * camSpeed;
			if(PlayerInput::DownPressed) 
				position -= forward * camSpeed;
			if(PlayerInput::LeftPressed) 
				position -= right * camSpeed;
			if(PlayerInput::RightPressed) 
				position += right * camSpeed;
		}

		// Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
		void ProcessMouseMovement(GLboolean constrainPitch = true)
		{
			float yoffset = PlayerInput::mousePositionDelta.y;
			float xoffset = PlayerInput::mousePositionDelta.x;
			
			if (abs(yoffset) > 0.1f)
				pitch += PlayerInput::mousePositionDelta.y * mouseSensitivity;
			if (abs(xoffset) > 0.01f)
				yaw += PlayerInput::mousePositionDelta.x * mouseSensitivity;
			yaw = glm::mod( yaw , 360.0f );
			if (PlayerInput::ZPressed)
				__debugbreak();
			// Make sure that when pitch is out of bounds, screen doesn't get flipped
			if (constrainPitch)
			{
				if (this->pitch > 89.0f)
					this->pitch = 89.0f;
				if (this->pitch < -89.0f)
					this->pitch = -89.0f;
			}


			// Update forward, Right and Up Vectors using the updated Eular angles
			this->updateCameraVectors();
		}

		// Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
		void ProcessMouseScroll(GLfloat yoffset)
		{
			if (this->zoom >= 1.0f && this->zoom <= 45.0f)
				this->zoom -= yoffset;
			if (this->zoom <= 1.0f)
				this->zoom = 1.0f;
			if (this->zoom >= 45.0f)
				this->zoom = 45.0f;
		}

	private:
		// Calculates the front vector from the Camera's (updated) Eular Angles
		void updateCameraVectors()
		{
			// Calculate the new forward vector
			glm::vec3 front;
			front.x = cos(radians(this->yaw)) * cos(radians(this->pitch));
			front.y = sin(radians(this->pitch));
			front.z = sin(radians(this->yaw)) * cos(radians(this->pitch));
			this->forward = normalize(front);
			// Also re-calculate the Right and Up vector
			this->right = normalize(cross(this->forward, this->worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
			this->up    = normalize(cross(this->right, this->forward));
		}
	};	
}
