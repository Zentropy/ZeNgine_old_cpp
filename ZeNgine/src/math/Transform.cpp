﻿#include "stdafx.h"
#include "Transform.h"

namespace Zen 
{
	void Transform::SetPosition(Vector3 inPosition) {
		position.x = inPosition.x;
		position.y = inPosition.y;
		position.z = inPosition.z;
		matrixIsDirty = true;
	}

	void Transform::SetPosition(float x, float y, float z) {
		position.x = x;
		position.y = y;
		position.z = z;
		matrixIsDirty = true;
	}

	Vector3 Transform::GetPosition() { return position;}

	void Transform::SetRotation(Quaternion inRotation) {
		rotation = inRotation;
		matrixIsDirty = true;
	}

	Quaternion Transform::GetRotation() { return rotation;}

	void Transform::SetScale(Vector3 inScale) {
		scale.x = inScale.x;
		scale.y = inScale.y;
		scale.z = inScale.z;
		matrixIsDirty = true;
	}

	void Transform::SetScale(float x, float y, float z) {
		scale.x = x;
		scale.y = y;
		scale.z = z;
		matrixIsDirty = true;
	}

	Vector3 Transform::GetScale() {
		return scale;
	}

	//Matrix4 Transform::GetTransformMatrix() {
	//	if (matrixIsDirty) {
	//		transformMatrix = scale 
	//	}
	//}
}
