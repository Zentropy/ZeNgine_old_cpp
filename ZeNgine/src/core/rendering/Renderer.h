#pragma once
#include "core/rendering/tuts/TutTriangle.h"

namespace Zen
{
	class Renderer
	{
	public:
		//TutTriangle tutTriangle;
		Renderer(GLFWwindow* window);
		~Renderer();
		//void BindDefaultVertexArray();
		void Init();
		void Render();

		//GLuint VAO, VBO, shaderProgram;
		GLFWwindow* window;
		//float vertices[9];
		vector<unique_ptr<IRenderable>> renderList;
	};

	
}
